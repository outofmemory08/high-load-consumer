package com.visma.task.consumer.controller;

import com.visma.task.consumer.events.MessageBroker;
import com.visma.task.consumer.events.ProcessedConsumer;
import com.visma.task.consumer.model.Status;
import com.visma.task.consumer.service.ProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class RestPublicController {

    @Autowired
    private ProcessingService processingService;

    @Autowired
    private ProcessedConsumer processedConsumer;

    @Autowired
    private MessageBroker messageBroker;

    /**
     * Adds create request to creation request queue
     *
     * @return temporary unique id of create request
     */
    @PostMapping("/init")
    public ResponseEntity<String> init() {
        return ResponseEntity.ok(processingService.processInit());
    }

    /**
     * Returns actual record unique id if create request is processed
     *
     * @param tmpUUID temporary unique id
     * @return {@link Status} object with actual record id if create request is processed
     */
    @GetMapping("/getCreationId/{tmpUUID}")
    public ResponseEntity<Status> getCreationId(@PathVariable String tmpUUID) {
        return ResponseEntity.ok(processedConsumer.checkCreated(tmpUUID));
    }

    /**
     * Returns status of actual record in third party service
     *
     * @param uuid unique id in third party service
     * @return {@link Status} object with status in third party service
     */
    @GetMapping("/checkStatus/{uuid}")
    public ResponseEntity<Status> checkStatus(@PathVariable String uuid) {
        return ResponseEntity.ok(processingService.getStatus(uuid));
    }

    /**
     * Gets status of request processing queues
     *
     * @return number of records in newRecord and processed queues
     */
    @GetMapping("/queueStatus")
    public ResponseEntity<String> queueStatus() {
        return ResponseEntity.ok(messageBroker.getQueueStatus());
    }

}
