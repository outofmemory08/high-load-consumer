package com.visma.task.consumer.events;

import com.visma.task.consumer.model.Status;
import com.visma.task.consumer.model.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Processed record queue producer component
 */
@Component
public class ProcessedConsumer {

    @Autowired
    private MessageBroker messageBroker;

    /**
     * Checks if record is created in third party service
     *
     * @param tmpUUID temporary unique id of create request
     * @return {@link Status} object with actual record id - if create request is processed and corresponding status type
     */
    public Status checkCreated(String tmpUUID) {
        String uuid = messageBroker.pollProcessed(tmpUUID);
        Status status = new Status();
        if (uuid != null) {
            status.setUuid(uuid);
            status.setStatusType(StatusType.OK);
        } else {
            status.setStatusType(StatusType.IN_PROGRESS);
        }
        return status;
    }

}