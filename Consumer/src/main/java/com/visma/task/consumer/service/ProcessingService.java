package com.visma.task.consumer.service;

import com.visma.task.consumer.events.NewRecordProducer;
import com.visma.task.consumer.model.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ProcessingService {

    private static final String URL_INIT = "http://localhost:8037/thirdpartyservice/init";
    private static final String URL_GET = "http://localhost:8037/thirdpartyservice/checkStatus/{uuid}";

    private final RestfulService restfulService;

    @Autowired
    private NewRecordProducer producer;

    public ProcessingService(RestfulService restfulService) {
        this.restfulService = restfulService;
    }

    /**
     * Creates temporary unique id and creates record in new record queue
     * @return temporary unique id of create request
     */
    public String processInit(){
        String tempUUID = UUID.randomUUID().toString();
        producer.addRecord(tempUUID);
        return tempUUID;
    }

    public String callInit() {
        String content = "content";
        ResponseEntity<String> response = restfulService.postJson(URL_INIT, content, String.class);
        return response.getBody();
    }

    public Status getStatus(String uuid) {
        ResponseEntity<Status> response = restfulService.get(URL_GET, Status.class, uuid);
        return response.getBody();
    }

}