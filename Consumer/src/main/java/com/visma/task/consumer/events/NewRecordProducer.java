package com.visma.task.consumer.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * New record queue producer component
 */
@Component
public class NewRecordProducer {

    @Autowired
    private MessageBroker messageBroker;

    public void addRecord(String tempUUID){
        messageBroker.addUnprocessed(tempUUID);
    }

}