package com.visma.task.consumer.events;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Simple message broker implementation
 */
@Component
public class MessageBroker {
    /**
     * Queue for unprocessed third party service requests
     * contains temporary unique ids only
     */
    private ConcurrentLinkedQueue<String> newRecordQueue = new ConcurrentLinkedQueue<>();
    /**
     * Queue for processed third party service requests
     * contains temporary unique id and actual unique id in third party system pairs
     */
    private ConcurrentHashMap<String, String> processedQueue = new ConcurrentHashMap<>();

    public void addUnprocessed(String tmpUUID) {
        newRecordQueue.add(tmpUUID);
    }

    public boolean hasUnprocessed() {
        return !newRecordQueue.isEmpty();
    }

    public String pollUnprocessed() {
        return newRecordQueue.poll();
    }

    public void addProcessed(String tmpUUID, String uuid) {
        processedQueue.put(tmpUUID, uuid);
    }

    public String pollProcessed(String tmpUUID) {
        String uuid = processedQueue.get(tmpUUID);
        if (uuid != null) {
            processedQueue.remove(tmpUUID);
        }
        return uuid;
    }

    public String getQueueStatus() {
        return " unprocessed records: " + newRecordQueue.size() +
                " processed records: " + processedQueue.size();
    }

}