package com.visma.task.consumer.events;

import com.visma.task.consumer.service.ProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Processed record queue consumer component
 */
@Component
public class ProcessedProducer {

    private static final Logger logger = LoggerFactory.getLogger(ProcessedProducer.class);

    @Autowired
    private MessageBroker messageBroker;

    @Autowired
    private ProcessingService processingService;

    /**
     * Asynchronously calls third party service /init method
     * Pool size of async threads is defined in application properties
     */
    @Async
    public void processInit(){
        if (messageBroker.hasUnprocessed()) {
            String tmpUUID = messageBroker.pollUnprocessed();
            try {
                String uuid = processingService.callInit();
                messageBroker.addProcessed(tmpUUID, uuid);
            } catch (Exception e) {
                messageBroker.addUnprocessed(tmpUUID);
                logger.error("Failed to complete third party /init request");
            }
        }
    }

}