package com.visma.task.consumer.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * New record queue consumer component
 */
@Component
public class NewRecordConsumer {

    @Autowired
    private MessageBroker messageBroker;

    @Autowired
    private ProcessedProducer processedProducer;

    @Scheduled(fixedDelay = 1)
    public void consume() {
        if (messageBroker.hasUnprocessed()) {
            processedProducer.processInit();
        }
    }

}